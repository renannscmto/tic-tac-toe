import React, { useState } from "react";
import Board from "./Board";

export default function Game() {
    const [history, setHistory] = useState([Array(9).fill(null)]); // Cria um tabuleiro a fim de guardar todos os movimentos para que seja possivel voltar as jogadas.
    const [currentMove, setCurrentMove] = useState(0);
    const xIsNext = currentMove % 2 === 0;       // Determina qual jogador que deverá jogar.
    const currentSquares = history[currentMove]; // Obtém o estado atual do tabuleiro com base no número de movimentos (currentMove) a partir do histórico (history).

    function handlePlay(nextSquares) {
        const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
        setHistory(nextHistory);
        setCurrentMove(nextHistory.length - 1)
    };

    function jumpTo(nextMove) {
        setCurrentMove(nextMove);
    };

    const moves = history.map((squares, move) => {
        let text = '';
        move > 0 ?  text = 'Go to move #' + move : text = 'Go to game start';
       
        return (
            <li key={move}>
                <button onClick={() => jumpTo(move)}>{text}</button>
            </li>
        );
    });

    return (
        <div className="game">
            <div className="game-board">
                <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} /> {/* Passando como Props os valores que serão usandos em Board */}
            </div>
            <div className="game-info">
                <ol>
                    {moves}
                </ol>
            </div>

        </div>
    );
};