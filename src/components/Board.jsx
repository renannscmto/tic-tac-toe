import React, { useState, useEffect } from "react";
import Square from "./Square";


export default function Board({xIsNext, squares, onPlay}) {
    const [status, setStatus] = useState("")

    function handleClick(i) {
        const nextSquares = squares.slice(); // Separando cada elemento do array com 'slice()', logo em seguida é adicionado o valor de "X" no indice clicado.

        if(squares[i] || calculateWinner(squares)) {  // Vericando se o 'Square' clicado está vazio ou se já existe um vencedor.
            return;
        };

        xIsNext ? nextSquares[i] = "O" : nextSquares[i] = "X"; // Verificando qual jogador deverá jogar.

        onPlay(nextSquares);
    };

    // Usando o useEffect para rederizar o componente somente quando os valores em conchete mudar.
    useEffect(() => {
        const winner = calculateWinner(squares);
        winner ? setStatus("Winner: " + winner) : setStatus("Next Player: " + (xIsNext ? "O" : "X"));
    }, [squares, xIsNext]);

    return (
        <>  
            <div className="status">{status}</div>
            <div className="board-row">
                <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
                <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
                <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
            </div>

            <div className="board-row">
                <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
                <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
                <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
            </div>

            <div className="board-row">
                <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
                <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
                <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
            </div>
        </>
    )
}

function calculateWinner(squares) {
    // Todas as formas para vencer estão neste array de arrays.
    const lines = [   
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];

    // Passando por todas as formas de vencer para saber se há um vencedor.
    for(let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        
        if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        };
    };

    return null;
}